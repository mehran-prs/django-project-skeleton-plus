# Release notes

## Unreleased
### Changed
* Changed `doc` folder name to `docs`

### Added
* log `formatter` for logger settings
* `Authentcate_password_validators` setting

### Change
* Override `django` logger setting to add file
 logger to it.

## [v2.0.2 (2019-08-17)](https://gitlab.com/mehran-prs/django-project-skeleton/tree/v2.0)

### Fixed
* Added `mysqlclient` to `requirements.txt` file.
* Fixed `PROJECT_ROOT` variable.

## [v2.0 (2019-08-17)](https://gitlab.com/mehran-prs/django-project-skeleton/tree/v2.0)
  
**Hard Forked from v1.0 of [base project](https://github.com/Mischback/django-project-skeleton)**

### Added
* Settings folder contain settings.
* Use .env to store sensitive data
* `.dockerignore` file in folders 
that need to ignore data

### Changed
* `docs` folder is empty and you can store your 
docs in way that you wnat.
* Now we have file log channel that store logs in 
`storage/logs`
* media store in `storage/media`
compiled statics store in `storage/static` (and 
you should have a soft link to this folder
to show to users.)
* `urls.py` file contain just admin url.

### Removed
* `.editorconfig`
* `flake8` config
* `.tox` config
