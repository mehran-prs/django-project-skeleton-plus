# install decouple pip package.
from decouple import Csv
from .utils import env

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': env('DB_NAME', '{{ project_name }}'),
        'USER': env('DB_USERNAME'),
        'PASSWORD': env('DB_PASSWORD'),
        'HOST': env('DB_HOST'),
        'PORT': env('DB_PORT', '3306'),
        'OPTIONS': {
            'charset': 'utf8mb4',
        },

        # Prevent close connection from mysql side,
        # (django should reopen connection earlier
        # than mysql throw it)
        # See this link: https://stackoverflow.com/questions/26958592/django-after-upgrade-mysql-server-has-gone-away
        'CONN_MAX_AGE': 500,
    }
}
