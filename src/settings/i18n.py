from os.path import join

from decouple import Csv
from .utils import env

from .base import PROJECT_ROOT, MIDDLEWARE

# -------------------------------------
#   Internationalization
# -------------------------------------

LANGUAGE_CODE = env('LANGUAGE_CODE', 'en-us')

TIME_ZONE = env('TIME_ZONE', 'UTC')

USE_I18N = True

USE_L10N = True

USE_TZ = True

LOCALE_PATHS = (
    join(PROJECT_ROOT, 'locale'),
)
