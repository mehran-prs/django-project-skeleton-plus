from os.path import join
from .utils import PROJECT_ROOT

# template stuff
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': join(PROJECT_ROOT, 'templates'),
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages'
            ],
        },
    },
]

# Static and media files (CSS, JavaScript, Images and uploaded files)
STATIC_URL = '/static/'
MEDIA_URL = '/media/'

STATIC_ROOT = join(PROJECT_ROOT, 'public', 'static')
MEDIA_ROOT = join(PROJECT_ROOT, 'storage', 'media')

STATICFILES_DIRS = [
    join(PROJECT_ROOT, 'static'),
]

PROJECT_TEMPLATES = [
    join(PROJECT_ROOT, 'templates'),
]

# -------------------------------------
#   Config Logging
# -------------------------------------
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': join(PROJECT_ROOT, 'storage', 'logs/log.log'),
            'formatter': 'standard',
            'backupCount': 3,
            'encoding': 'UTF-8',
            'when': 'D',
        },
        'console': {
            'level': 'INFO',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        # Project src logger
        'src': {
            'handlers': ['console', 'file', 'mail_admins'],
            'level': 'DEBUG',
        },

        # Django logger
        'django': {
            'handlers': ['console', 'file', 'mail_admins'],
            'level': 'INFO',
        },

        # werkzeug server logger
        'werkzeug': {
            'handlers': ['console', 'file'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },

}
