from os.path import abspath, dirname

PROJECT_ROOT = dirname(dirname(dirname(abspath(__file__))))

env = AutoConfig(PROJECT_ROOT)
