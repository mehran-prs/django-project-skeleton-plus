from os.path import join
from .utils import PROJECT_ROOT

# install decouple pip package.
from decouple import Csv
from .utils import env

DEBUG = env('DEBUG', default=False, cast=bool)
TEMPLATE_DEBUG = env('TEMPLATE_DEBUG', default=False, cast=bool)

SECRET_KEY = env('SECRET_KEY')

ALLOWED_HOSTS = env('ALLOWED_HOSTS', cast=Csv())

# Fixme: set this values in .env and get by env method.
ADMINS = (
    ('your name', 'your_name@example.com'),
)

INTERNAL_IPS = [
    '127.0.0.1',
]

MANAGERS = ADMINS

ROOT_URLCONF = 'src.{{ project_name }}.urls'
WSGI_APPLICATION = 'src.{{ project_name }}.wsgi.application'

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Example:
    # 'src.apps.myapp'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
